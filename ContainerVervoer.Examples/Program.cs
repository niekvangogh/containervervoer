﻿using System;
using System.Collections.Generic;

namespace ContainerVervoer.Examples
{
    public class Program
    {
        static void Main(string[] args)
        {
            var shipManager = new ContainerShipManager();

            var containers = new List<Container>
            {
                new Container(4000, false, false),
                new Container(8000, false, false),
                new Container(4000, true, true),
                new Container(6400, false, true),
                new Container(4000, false, false),
                new Container(5600, true, false),
                new Container(6000, false, false),
                new Container(4000, false, true),
                new Container(25000, true, true),
                new Container(4000, false, false),
                new Container(11630, false, false),
                new Container(4000, false, true),
                new Container(3250, true, false),
                new Container(20000, false, false),
                new Container(4000, false, false),
                new Container(8000, false, false),
                new Container(4000, true, true),
                new Container(6400, false, true),
                new Container(4000, false, false),
                new Container(5600, true, false),
                new Container(6000, false, false),
                new Container(4000, false, true),
                new Container(25000, true, true),
                new Container(4000, false, false),
                new Container(11630, false, false),
                new Container(4000, false, true),
                new Container(3250, true, false),
                new Container(20000, false, false),
                new Container(4000, false, false),
                new Container(8000, false, false),
                new Container(4000, true, true),
                new Container(6400, false, true),
                new Container(4000, false, false),
                new Container(5600, true, false),
                new Container(6000, false, false),
                new Container(4000, false, true),
                new Container(25000, true, true),
                new Container(4000, false, false),
                new Container(11630, false, false),
                new Container(4000, false, true),
                new Container(3250, true, false),
                new Container(20000, false, false),
                new Container(4000, false, false),
                new Container(8000, false, false),
                new Container(4000, true, true),
                new Container(6400, false, true),
                new Container(4000, false, false),
                new Container(5600, true, false),
                new Container(6000, false, false),
                new Container(4000, false, true),
                new Container(25000, true, true),
                new Container(4000, false, false),
                new Container(11630, false, false),
                new Container(4000, false, true),
                new Container(3250, true, false),
                new Container(20000, false, false),
                new Container(4000, false, false),
                new Container(8000, false, false),
                new Container(4000, true, true),
                new Container(6400, false, true),
                new Container(4000, false, false),
                new Container(5600, true, false),
                new Container(6000, false, false),
                new Container(4000, false, true),
                new Container(25000, true, true),
                new Container(4000, false, false),
                new Container(11630, false, false),
                new Container(4000, false, true),
                new Container(3250, true, false),
                new Container(20000, false, false)
            };

            shipManager.Containers.AddRange(containers);
            shipManager.SortContainers();
        }
    }
}