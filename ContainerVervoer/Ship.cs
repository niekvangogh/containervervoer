using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using ContainerVervoer.Utils;

namespace ContainerVervoer
{
    public class Ship
    {
        public ContainerStack[,] ContainersGrid { get; }

        public int Length { get; }

        public Ship(int width, int length)
        {
            Length = length;
            ContainersGrid = new ContainerStack[width, length];
            for (var x = 0; x < width; x++)
            {
                for (var y = 0; y < length; y++)
                {
                    ContainersGrid[x, y] = new ContainerStack(x <= 1);
                }
            }
        }

        /// <summary>
        /// Tries placing containers somewhere within the ship
        /// </summary>
        /// <param name="containers"></param>
        /// <returns>Return the containers which were not placed</returns>
        public List<Container> TryPlace(params Container[] containers)
        {
            return (from container in containers let isPlaced = CanPlace(container) where !isPlaced select container)
                .ToList();
        }

        /// <summary>
        /// Checks if there is a place where the container can be placed.
        /// </summary>
        /// <param name="container"></param>
        /// <returns>Returns a boolean, which means if it has been placed or not</returns>
        public bool CanPlace(Container container)
        {
            var index = GetPlacable(container);
            return index != null;
        }

        /// <summary>
        /// Gets the placable ContainerStack for the container to be placed within the ship.
        /// </summary>
        /// <param name="container"></param>
        /// <returns>Returns the ContainerStack where there is space for the container</returns>
        public ContainerStack GetPlacable(Container container)
        {
            var shipSides = ShipUtils.GetShipSides(this);

            var leftWeight = ShipUtils.GetContainerStackWeight(shipSides[0].ToArray());
            var rightWeight = ShipUtils.GetContainerStackWeight(shipSides[1].ToArray());

            var insertSide = leftWeight > rightWeight ? shipSides[1].ToList() : shipSides[0].ToList();

            return insertSide.First(stack => stack.TryPlace(container));
        }
    }
}