using System;
using System.Collections.Generic;
using System.Linq;

namespace ContainerVervoer
{
    public class ContainerShipManager
    {
        public List<Container> Containers { get; }

        public List<Ship> Ships { get; }

        public ContainerShipManager()
        {
            Ships = new List<Ship>();
            Containers = new List<Container>();
        }

        private Ship CreateShip(int width = 5, int height = 10)
        {
            var ship = new Ship(width, height);
            Ships.Add(ship);
            return ship;
        }

        public void SortContainers()
        {
            foreach (var container in Containers)
            {
                var placed = Ships.Any(ship => ship.CanPlace(container));
                if (!placed)
                {
                    CreateShip().CanPlace(container);
                }
            }

            Console.Write("");
        }
    }
}