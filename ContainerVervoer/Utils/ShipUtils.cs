using System;
using System.Collections.Generic;
using System.Linq;

namespace ContainerVervoer.Utils
{
    public static class ShipUtils
    {
        
        /// <summary>
        /// Returns a list with side of the ship 
        /// </summary>
        /// <param name="ship"></param>
        /// <returns>Returns 2 lists where the first item is represented to be the left side, and the second item the right side</returns>
        public static List<List<ContainerStack>> GetShipSides(Ship ship)
        {
            var shipWidth = ship.ContainersGrid.GetLength(0);
            var centerIndex = (int) Math.Floor((decimal) shipWidth / 2);

            var isEven = shipWidth % 2 == 0;

            List<ContainerStack> leftSide, rightSide;
            if (isEven)
            {
                leftSide = GetContainersFromSide(ship, 0, centerIndex);
                rightSide = GetContainersFromSide(ship, centerIndex, shipWidth);
            }
            else
            {
                leftSide = GetContainersFromSide(ship, 0, centerIndex);
                rightSide = GetContainersFromSide(ship, centerIndex + 1, shipWidth);
            }

            return new List<List<ContainerStack>>(new[] {leftSide, rightSide});
        }

        public static List<ContainerStack> GetContainersFromSide(Ship ship, int startColumn, int endColumn)
        {
            var containers = new List<ContainerStack>();
            for (var x = startColumn; x < endColumn; x++)
            {
                for (var y = 0; y < ship.Length; y++)
                {
                    containers.Add(ship.ContainersGrid[x, y]);
                }
            }

            return containers;
        }

        public static int GetContainerStackWeight(params ContainerStack[] stacks)
        {
            return stacks.Sum(containerStack => containerStack.Weight);
        }

        public static int GetTotalShipWeight(Ship ship)
        {
            return ship.ContainersGrid.Cast<ContainerStack>().Sum(stack => stack.Weight);
        }

        public static int GetMaxShipWeight(Ship ship)
        {
            return ship.ContainersGrid.Cast<ContainerStack>()
                .Sum(stack => stack.Containers.Sum(container => container.MaxWeight));
        }
    }
}