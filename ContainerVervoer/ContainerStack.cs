using System;
using System.Collections.Generic;
using System.Linq;

namespace ContainerVervoer
{
    public class ContainerStack
    {
        public List<Container> Containers { get; }

        public int Weight { get; private set; }

        public bool IsCooled { get; }

        public ContainerStack(bool isCooled)
        {
            Containers = new List<Container>();
            IsCooled = isCooled;
        }

        /// <summary>
        /// Gets the placable index for in the ContainerStack
        /// </summary>
        /// <param name="container"></param>
        /// <returns>Returns the index for where in the ContainerStack the container can fit, if it returns -1, it means it can't be placed and if it returns anything else, it will be placed at that index in the ContainerStack</returns>
        public int GetPlacableIndex(Container container)
        {
            var newWeight = Weight + container.CurrentWeight;
            if (newWeight > 150000)
            {
                return -1;
            }

            if (container.NeedsCooling)
            {
                if (!IsCooled)
                {
                    return -1;
                }
            }

            if (Containers.Count != 0)
            {
                if (Containers.Last().IsValuable && container.IsValuable)
                {
                    return -1;
                }

                if (Containers.Last().IsValuable)
                {
                    return 0;
                }
            }

            return Containers.Count;
        }

        
        /// <summary>
        /// Tries placing the container in the container stack
        /// </summary>
        /// <param name="container"></param>
        /// <returns>Returns true if it was placed, returns false if it could not be placed in the ContainerStack</returns>
        public bool TryPlace(Container container)
        {
            var index = GetPlacableIndex(container);

            if (index == -1)
            {
                return false;
            }

            Place(container, index);
            return true;
        }

        /// <summary>
        /// Places the container on the ship and adds the weight to the ContainerStack
        /// </summary>
        /// <param name="container"></param>
        /// <param name="index"></param>
        private void Place(Container container, int index)
        {
            Containers.Insert(index, container);
            Weight += container.CurrentWeight;
            Console.WriteLine(
                $"stack: weight: {Weight} height: {Containers.Count}; container index: {index}; ");
        }
    }
}