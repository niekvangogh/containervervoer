using System;
using System.Collections.Generic;
using System.Numerics;

namespace ContainerVervoer
{
    public class Container
    {
        public int CurrentWeight { get; }

        public bool IsValuable { get; }

        public bool NeedsCooling { get; }

        public int MaxWeight { get; }

        public Container(int contentWeight, bool valuable, bool needsCooling)
        {
            IsValuable = valuable;
            NeedsCooling = needsCooling;
            CurrentWeight = contentWeight;
            MaxWeight = 30000;
        }
    }
}