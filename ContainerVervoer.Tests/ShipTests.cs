using System.Linq;
using ContainerVervoer.Utils;
using Xunit;

namespace ContainerVervoer.Tests
{
    public class ShipTest
    {
        [Fact]
        public void CheckIfStackWeightsNotTooMuch_GivenTooMuchWeight_ExpectsRemainingContainers()
        {
            var containerStack = new ContainerStack(true);
            var containers = new[]
            {
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
            };

            foreach (var container in containers)
            {
                containerStack.TryPlace(container);
            }

            Assert.True(containerStack.Weight <= 150000);
        }

        [Fact]
        public void CheckIfValuableContainersAreOnlyOnTop_GivenShip_ExpectsNoValuableContainersBelowTop()
        {
            var containerStack = new ContainerStack(false);
            var containers = new[]
            {
                new Container(30000, true, false),
                new Container(30000, true, false),
                new Container(30000, true, false),
                new Container(30000, true, false),
                new Container(30000, true, false),
                new Container(30000, true, false),
                new Container(30000, true, false),
                new Container(30000, true, false),
                new Container(30000, true, false)
            };

            foreach (var container in containers)
            {
                containerStack.TryPlace(container);
            }


            foreach (var container in containerStack.Containers.FindAll(container => container.IsValuable))
            {
                if (containerStack.Containers.Last() != container)
                {
                    Assert.True(false);
                }
            }

            Assert.True(true);
        }

        [Fact]
        public void
            CheckIfCooledContainersHasCooledContainerStack_GivenCooledContainers_ExpectsAllCooledContainersBeingCooled()
        {
            var containerStack = new ContainerStack(true);
            var containers = new[]
            {
                new Container(4000, true, false),
                new Container(4000, false, false),
                new Container(4000, false, false)
            };

            foreach (var container in containers)
            {
                containerStack.TryPlace(container);
            }


            foreach (var container in containerStack.Containers.FindAll(container => container.NeedsCooling))
            {
                if (container.NeedsCooling)
                {
                    if (!containerStack.IsCooled)
                    {
                        Assert.True(false);
                    }
                }
            }

            Assert.True(true);
        }

        [Fact]
        public void CheckBalancing_GivenNotFullShip_ShouldReturn5050With20PercentMargin()
        {
            var ship = new Ship(4, 2);
            var containers = new[]
            {
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false)
            };

            ship.TryPlace(containers);

            var leftSide = ShipUtils.GetShipSides(ship)[0].ToArray();
            var rightSide = ShipUtils.GetShipSides(ship)[1].ToArray();

            var leftWeight = ShipUtils.GetContainerStackWeight(leftSide);
            var rightWeight = ShipUtils.GetContainerStackWeight(rightSide);

            var percentile = leftWeight / rightWeight;

            Assert.True(percentile > .8);
            Assert.True(percentile < 1.2);
        }

        [Fact]
        public void CheckWeight_Is50OrMorePercentUsed_ReturnsTrue()
        {
            var ship = new Ship(2, 2);
            var containers = new[]
            {
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false),
                new Container(30000, false, false)
            };
            ship.TryPlace(containers);

            var maxWeight = ShipUtils.GetMaxShipWeight(ship);
            var shipWeight = ShipUtils.GetTotalShipWeight(ship);
            Assert.True(maxWeight / 2 < shipWeight);
        }
    }
}